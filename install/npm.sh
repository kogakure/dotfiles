 #!/bin/sh

# Runs an accessibility audit against a URL
npm install -g a11y

# Catch insensitive, inconsiderate writing.
npm install -g alex

# Babel command line.
npm install -g babel-cli

# Custom parser for ESLint
npm install -g babel-eslint

# A Babel preset for each environment.
npm install babel-preset-env

# BackstopJS: Catch CSS curveballs.
npm install -g backstopjs

# Browser package manager
npm install -g bower

# Find broken links, missing images, etc in your HTML
# ➜ blc https://www.stefanimhoff.de
npm install -g broken-link-checker

# Live CSS Reload & Browser Syncing
npm install -g browser-sync

# caniuse for the command line
# ➜ caniuse border-radius
npm install -g caniuse-cmd

# Git commit, but play nice with conventions.
# ➜ git cz
npm install -g commitizen

# Canvas graphics API backed by Cairo
npm install -g canvas

# Prompts for conventional changelog standard.
npm install -g cz-conventional-changelog

# CSSO (CSS Optimizer) is a CSS minifier with structural optimisations
# ➜ csso in.css out.css
npm install -g csso

# runs Node.js programs through Chromium DevTools
# ➜ devtool src/app.js
npm install -g devtool

# An AST-based pattern checker for JavaScript.
npm install -g eslint
npm install -g eslint-plugin-babel
npm install -g eslint-plugin-chai
npm install -g eslint-plugin-chai-expect
npm install -g eslint-plugin-flowtype
npm install -g eslint-plugin-html
npm install -g eslint-plugin-jasmine
npm install -g eslint-plugin-jsdoc
npm install -g eslint-plugin-json
npm install -g eslint-plugin-jsx
npm install -g eslint-plugin-jsx-extras
npm install -g eslint-plugin-lodash
npm install -g eslint-plugin-mocha
npm install -g eslint-plugin-node
npm install -g eslint-plugin-react
npm install -g eslint-plugin-redux

# Express' application generator
# ➜ express myapp --hogan -c less
npm install -g express-generator

# Test your download speed using fast.com
# ➜ fast
npm install -g fast-cli

# Command-Line Interface for Firebase
npm install -g firebase-tools

# Library for streamlining application deployment or systems administration tasks
# ➜ fly
npm install -g flightplan

# Binary wrapper for Flow - A static type checker for JavaScript
npm install -g flow-bin

# Just the node wrapper for the Galen Framework.
# ➜ galen
npm install -g galenframework-cli

# React.js Static Site Generator
npm install -g gatsby

# Convert any video file to an optimized animated GIF
npm install -g gifify

# CLI to generate books and documentation using gitbook
npm install -g gitbook-cli

# Your web font utility belt
npm install -g glyphhanger

# GraphQL CLI
npm install -g graphql-cli

# Command line interface for gulp
npm install -g gulp-cli

# Hops
npm install -g hops-cli

# A simple zero-configuration command-line http server
npm install -g http-server

# Small JSON file server for REST API mock
# ➜ touch db.json; json-server --watch db.json
npm install -g json-server

# Tool for managing JavaScript projects with multiple packages (BETA)
npm install -g lerna@^2.0.0-beta

# Lighthouse analyzes web apps and web pages, collecting modern performance metrics and insights on developer best practices.
npm install -g lighthouse

# simple, flexible, fun test framework
npm install -g mocha

# Wrap web apps natively
npm install -g nativefier

# The Netlify CLI tools lets you create, deploy, and delete new sites straight from your terminal.
npm install -g netlify-cli

# Tool to create icns files for OSX
npm install -g node-icns

# Simple monitor script for use during development of a node.js app.
npm install -g nodemon

# Check for outdated, incorrect, and unused dependencies.
# ➜ npm-check -g
npm install -g npm-check

# Find newer versions of dependencies than what your package.json or bower.json allows
# ➜ ncu
npm install -g npm-check-updates

# Executes <command> either from a local node_modules/.bin, or from a central cache, installing any packages needed in order for <command> to run.
npm install -g npx

# Capture website screenshots
# ➜ pageres [ todomvc.com 1200x1000 ] [ yeoman.io 800x600 1200x1000 ] --crop
npm install -g pageres-cli

# Single-Command Node.js Compiler
npm install -g pkg

# Start building a Preact Progressive Web App in seconds.
npm install -g preact-cli

# Prettier is an opinionated code formatter
npm install -g prettier
npm install -g prettier-stylelint

# Package for formatting JSON data in a coloured YAML-style, perfect for CLI output
# ➜ prettyjson package.json
# ➜ curl https://api.github.com/users/rafeca | prettyjson
npm install -g prettyjson

# CLI for postcss
npm install -g postcss-cli

# A commandline tool for Polymer projects
npm install -g polymer-cli

# Serverless Framework
npm install -g serverless

# Modern CSS linter
npm install -g stylelint
npm install -g stylelint-config-standard
npm install -g stylelint-config-prettier

# Analyze and debug space usage through source maps
npm install -g source-map-explorer

# Node.js module and command-line tool for exporting SVG to PNG and JPEG.
npm install -g svgexport

# Nodejs-based tool for optimizing SVG vector graphics files
npm install -g svgo

# tldr;
npm install -g tldr

# CLI tool for downloading your precious Tumblr likes.
npm install -g tumblr-lks-downldr-cli

# TypeScript is a language for application scale JavaScript development
npm install -g typescript

# JavaScript parser, mangler/compressor and beautifier toolkit
npm install -g uglify-js

# A simple CLI for scaffolding Vue.js projects.
npm install -g @vue/cli

# Packs CommonJs/AMD modules for the browser. Allows to split your codebase into multiple bundles, which can be loaded on demand. Support loaders to preprocess files, i.e. json, jade, coffee, css, less, ... and your custom stuff.
npm install -g webpack

# Serves a webpack app. Updates the browser on changes.
npm install -g webpack-dev-server

# Fast, reliable, and secure dependency management.
npm install -g yarn

# Yeoman
npm install -g yo
npm install -g generator-webapp
npm install -g generator-alfred
