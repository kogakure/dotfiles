#!/bin/sh

############
### Taps ###
############

brew tap caskroom/cask
brew tap caskroom/versions
brew tap caskroom/drivers

# XQuartz needs to be installed first
brew cask install xquartz

#############
### Casks ###
#############

# brew cask install 1password # Install from Store for TouchID
brew cask install alfred
brew cask install appzapper
brew cask install bartender
brew cask install blender
brew cask install brave
brew cask install calibre
brew cask install ccmenu
brew cask install color-oracle
brew cask install cryptomator
brew cask install daisydisk
brew cask install dash
brew cask install delicious-library
brew cask install devonthink-pro-office
brew cask install dropbox
brew cask install evernote
brew cask install firefox
brew cask install firefoxdeveloperedition
brew cask install firefoxnightly
brew cask install flip4mac
brew cask install fontexplorer-x-pro
brew cask install fontforge
brew cask install gemini
brew cask install gitbook-editor
brew cask install github-desktop
brew cask install google-chrome
brew cask install google-chrome-beta
brew cask install google-chrome-canary
brew cask install google-chrome-dev
brew cask install google-drive
brew cask install google-earth-pro
brew cask install google-earth-web-plugin
brew cask install google-hangouts
brew cask install google-japanese-ime
brew cask install google-photos-backup
brew cask install hammerspoon
brew cask install handbrake
brew cask install hipchat
brew cask install imagealpha
brew cask install imageoptim
brew cask install integrity
brew cask install iterm2
brew cask install kaleidoscope
brew cask install kindle
brew cask install lastfm
brew cask install livereload
brew cask install macvim
brew cask install marked
brew cask install mousepose
brew cask install mysqlworkbench
brew cask install omnigraffle6
brew cask install opera
brew cask install opera-beta
brew cask install opera-developer
brew cask install origin
brew cask install owncloud
brew cask install p4merge
brew cask install postman
brew cask install qlvideo
brew cask install riot
brew cask install robo-3t
brew cask install sequel-pro
brew cask install sitesucker
brew cask install skyfonts # Run installer later
brew cask install spotify
brew cask install spotify-notifications
brew cask install sqlitebrowser
brew cask install steam
brew cask install stellarium
brew cask install textmate
brew cask install the-unarchiver
brew cask install torbrowser
brew cask install transmit
brew cask install tunnelblick
brew cask install unrarx
brew cask install virtualbox
brew cask install visual-studio-code
brew cask install vlc
brew cask install vlc-webplugin
brew cask install wacom-bamboo-tablet
brew cask install wd-my-cloud
brew cask install webkit-nightly
brew cask install whatroute
brew cask install whatsapp
brew cask install xmind
